
module.exports = function(app) {
  var myPlaces = require('../controllers/myPlacesController');
  var cities = require('../controllers/citiesController');
  app.route('/myPlaces')
    .get(myPlaces.list_all_places)
    .post(myPlaces.create_a_new_place);

     app.route('/myPlacesByCity/:cityId')
    .get(myPlaces.list_all_places_by_city);
   
   app.route('/myPlaces/:id')
    .get(myPlaces.read_a_place)
    .put(myPlaces.update_a_place)
    .delete(myPlaces.delete_a_place);

    app.route('/cities')
    .get(cities.list_all_cities);
    };