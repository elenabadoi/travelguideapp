/*this will be executed from the command line to initialize
 the database in the mysql server.*/
DROP DATABASE IF EXISTS travelguide_db;
CREATE DATABASE travelguide_db;

USE travelguide_db;

CREATE TABLE cities
(
	id int NOT NULL AUTO_INCREMENT,
	city varchar(255) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE places_list
(
	id int NOT NULL AUTO_INCREMENT,
	place varchar(255) NOT NULL,
    cityId int NOT NULL,
	visited BOOLEAN DEFAULT false,
	PRIMARY KEY (id)
);

//inserts
  
INSERT INTO cities (city) VALUES ('Paris');
INSERT INTO cities (city) VALUES ('Londra');
INSERT INTO cities (city) VALUES ('Vienna');
INSERT INTO cities (city) VALUES ('Bucharest');
INSERT INTO cities (city) VALUES ('Praga');
INSERT INTO cities (city) VALUES ('Roma');
INSERT INTO cities (city) VALUES ('Barcelona');
INSERT INTO cities (city) VALUES ('Budapesta');
INSERT INTO cities (city) VALUES ('Berlin');
INSERT INTO cities (city) VALUES ('Dresden');
INSERT INTO cities (city) VALUES ('Brugge');
INSERT INTO cities (city) VALUES ('Amsterdam');
INSERT INTO cities (city) VALUES ('Brussels');

INSERT INTO places_list (place,cityId) VALUES ('Eiffel Tower', 1);
INSERT INTO places_list (place,cityId) VALUES ('Louvre Museum', 1);
INSERT INTO places_list (place,cityId, visited) VALUES ('Arc de Triomphe', 1, 1);
INSERT INTO places_list (place,cityId) VALUES ('Hyde Park', 2);
INSERT INTO places_list (place,cityId, visited) VALUES ('Tower of London', 2, 1);