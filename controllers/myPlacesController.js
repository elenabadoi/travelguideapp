
//importing the  model
var myPlaces = require("../models/myPlaces.js");

exports.list_all_places = function(req, res) {
    myPlaces.all(function(data) {
    var obj = {
        myPlaces: data
    };
    console.log(obj);
    res.status(200).json(obj);
  });
};

exports.create_a_new_place = function(req, res){
    myPlaces.create(["place", "cityId", "visited"],
    [req.body.place, req.body.cityId, req.body.visited],
    function() {
      res.status(202).json({message : 'accepted'})
  });
};

//update
exports.update_a_place = function(req, res) {
  var condition = "id = " + req.params.id;
  //console.log("condition", condition);

  var update = {
    cityId: req.body.cityId,
    place: req.body.place,
    visited: req.body.visited
  };

  myPlaces.update(update, condition, function() {
    res.status(202).json({message : 'accepted'})
  });
};

//delete
exports.delete_a_place = function(req, res) {
  var condition = "id = " + req.params.id;
  myPlaces.delete(condition, function() {
    res.status(202).json({message : 'accepted'})
  });
};

//select a place by id
exports.read_a_place = function(req, res) {
    var condition = "id = " + req.params.id;  
    myPlaces.getById(condition, function(data) {
      var obj = {
          myPlaces: data
      };
      console.log(obj);
      if (!obj){
        res.status(404).json({message : 'not found'})
      }
      else{
        res.status(200).json(obj)
      }
    });
  };

  //select a place by cityId
  exports.list_all_places_by_city = function(req, res) {
    var condition = "cityId = " + req.params.cityId;
    myPlaces.getById(condition, function(data) {
      var obj = {
          myPlaces: data
      };
      //console.log(obj);
      if (!obj){
        res.status(404).json({message : 'not found'})
      }
      else{
        res.status(200).json(obj)
      }
    });
  };
