//functions which needs a DB connection
var orm = require("../config/orm.js");

var myPlaces = {
  all: function(result) {
    orm.all("places_list", function(res) {
        result(res);
    });
  },
  getById: function(condition, result) {
    orm.getById("places_list", condition, function(res) {
        result(res);
    });
  },
  // Setting up arrays as columns and vals.
  create: function(cols, vals, result) {
    orm.create("places_list", cols, vals, function(res) {
        result(res);
    });
  },
  update: function(objColVals, condition, result) {
    orm.update("places_list", objColVals, condition, function(res) {
        result(res);
    });
  },
  delete: function(objColVals, condition, result) {
    orm.delete("places_list", objColVals, condition, function(res) {
        result(res);
    });
  }
};

/* Export the database functions for the controller- myPlacesController.*/
module.exports = myPlaces;