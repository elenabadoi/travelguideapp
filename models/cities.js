//functions which needs a DB connection
var orm = require("../config/orm.js");

var cities = {
  all: function(cb) {
    orm.all("cities", function(res) {
      cb(res);
    });
  },
  // Setting up arrays as columns and vals.
  create: function(cols, vals, cb) {
    orm.create("cities", cols, vals, function(res) {
      cb(res);
    });
  },
  update: function(objColVals, condition, cb) {
    orm.update("cities", objColVals, condition, function(res) {
      cb(res);
    });
  },
  delete: function(objColVals, condition, cb) {
    orm.delete("cities", objColVals, condition, function(res) {
      cb(res);
    });
  }
};

/* Export the database functions for the controller- citiesController.*/
module.exports = cities;