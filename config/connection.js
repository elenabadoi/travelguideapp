// DB connection.
var mysql = require("mysql");
var connection;

  connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "travelguide_db"
  });

  connection.connect(function(err) {
    if (err) {
        console.error('Error connecting: ' + err.stack);
        return;
    }

    console.log('Connected as id ' + connection.threadId);
});
// Export connection for ORM to use.
module.exports = connection;
