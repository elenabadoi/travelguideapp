var express = require('express');
var bodyParser = require("body-parser");//This is a node.js middleware for handling JSON, Raw, Text and URL encoded form data
//var methodOverride = require("method-override");
var app = express();

var path = require("path");

//app.use(express.static("public"));
app.use(express.static(path.join(__dirname + '.../public')));

port = process.env.PORT || 8080;

app.listen(port);
console.log('API server started on: ' + port);

// With 'false' the URL-encoded data is parsed with the querystring library.
app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());
app.use((req, res, next) => {
	console.warn(req.method + ' ' +req.url);
	next();
});

app.use((err, req, res, next) => {
    console.error(err);
    res.status(500).json({error: 'an error occurred'});
    });
    
var routes = require('./routes/appRoutes'); //importing route
routes(app); //register the route
