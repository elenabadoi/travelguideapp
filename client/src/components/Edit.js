import React, { Component } from "react";
import axios from 'axios';

const placesApi = 'http://localhost:8080/myPlaces/';

class Edit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            place: {}
        };
    }

    componentDidMount() {
      this.getAPlace();
    }

    getAPlace(){
      axios.get(placesApi + this.props.match.params.id)
      .then(res => {
        const place = res.data.myPlaces;
        console.log(place);
        this.setState({ place: place });
        return place;
      })
    }

    onChange = ({ target }) => {
      //this.setState({ [target.name]: target.value });
      const state = this.state.place     
      state[target.name] = target.value;
      this.setState({
        place: state
      });
   };

    onSubmit = (target) => {
        target.preventDefault();
        console.log(this.state);
      const {place,cityId,visited} = this.state.place;
      const body = {place,cityId,visited};

        axios.put(placesApi + this.props.match.params.id, {place,cityId,visited})
            .then((result) => {
                this.props.history.push("/")
            });
    }

  render() {
    const plc = this.state.place;
    console.log(plc);
return (
<div>
  <div className="jumbotron">
   
  </div>

  <div className="container">
    <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label htmlFor="title">Description:</label>
            <input type="text" className="form-control" name="place"  onChange={this.onChange} defaultValue={plc.place} />
          </div>
          <div className="form-group">
            <label htmlFor="author">City:</label>
            <input type="text" className="form-control" name="cityId" onChange={this.onChange} defaultValue={plc.cityId} />
          </div>
          <div className="form-group">
            <label htmlFor="author">Visited:</label>
            <input type="text" className="form-control" name="visited"  onChange={this.onChange} defaultValue={plc.visited} />
          </div>
          <button type="submit" className="btn btn-default">Submit</button>
    </form>
    
  </div>
</div>
    );
  }
}

export default Edit;