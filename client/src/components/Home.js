
import React, { Component } from "react";
import axios from 'axios';
import { Link } from 'react-router-dom';
import Select from 'react-select';

const placesApi = 'http://localhost:8080/myPlaces/';
const citiesApi = 'http://localhost:8080/cities/';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            places: [],
            cities: [],
            error: null
          }
      }
   
    componentDidMount() {
        this.getPlaces();
        this.getCities();
    }

    getCities(){
        axios.get(citiesApi)
        .then(res => {
          const cities = res.data.cities;
          this.setState({ cities: cities});
        })
      }

  getPlaces(){
    axios.get(placesApi)
    .then(res => {
      const places = res.data.myPlaces;
      this.setState({ places: places , isLoading: false });
    })
  }

    delete(id) {
        console.log(id);
        // state, before delete anything
        const crtPlaces = this.state.places;
        axios.delete(placesApi + id)
            .then((res => {
                // Remove deleted item from state.
                this.setState({
                    places: crtPlaces.filter(place => place.id !== id),
                });
                this.props.history.push("/");  
                this.forceUpdate();                                                                        
            })); 
    }

render() {
    const { isLoading, places, cities } = this.state;
    const options=
        this.state.cities.map((cty, index) => {
            return {
                label: cty.city,
                value: cty,
                key: index
            }
        });

    console.log(this.state);
        return (  
    <React.Fragment>       
        <div>
        <div className="jumbotron">
            <h2>Home</h2>
            <p className="lead">My List</p>
        </div>
        <Select
        options={options}
        />
        <div className="container">
            <div className="row">
                <Link to={`/create`} className="btn btn-outline-info">Add New</Link>
                <table className="table table-borderless">
                    <thead>
                        <tr>					
                            <th scope="col">Description</th>
                            <th scope="col">City</th>
                            <th scope="col">Visited</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    {!isLoading && places.length ? (
                this.state.places.map(plc => {
                    const { id, place, cityId, visited } = plc;
                    return (
                        <tr key={id}>					
                            <td>{place}</td>
                            <td>{cityId}</td>
                            <td>{visited}</td>
                            <td><Link to={`/edit/${id}`} className="btn btn-outline-info">Edit</Link></td>					
                            <td><button onClick={this.delete.bind(this, id)} className="btn btn-outline-danger">Delete</button></td>					
                        </tr>
                    );           
                })
                ) : (
                <tr><td>Loading...</td></tr>
                )}
            </tbody>
                </table>
            </div>
        </div>
        </div>
        </React.Fragment> 
            );
        }
}

export default Home;