import React, { Component } from "react";
import axios from 'axios';

const placesApi = 'http://localhost:8080/myPlaces/';

class Create extends Component {

    constructor() {
        super();
        this.state = {
            place: '',
            cityId: '',
            visited: ''
        };
    }

    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();
    
        const {place,cityId,visited} = this.state;

        axios.post(placesApi, {
                place,
                cityId,
                visited
            })
            .then((result) => {
                this.props.history.push("/")
            });
    }

render() {
  	const { place,cityId, visited } = this.state;
    return (
<div>
	<div className="jumbotron">
		<h2>Add new</h2>
		<p className="lead">Add new place in your list</p>
	</div>
	<div className="container">
		<div className="row">			
			<form onSubmit={this.onSubmit}>
				<div className="form-group">
					<label htmlFor="place">Description:</label>
					<input type="text" className="form-control" name="place" value={place} onChange={this.onChange} placeholder="Description" />
				</div>
				<div className="form-group">
					<label htmlFor="cityId">City:</label>
					<input type="text" className="form-control" name="cityId" value={cityId} onChange={this.onChange} placeholder="City" />
				</div>
                <div className="form-group">
					<label htmlFor="visited">Visited:</label>
					<input type="text" className="form-control" name="visited" value={visited} onChange={this.onChange} placeholder="Visited" />
				</div>
				<button type="submit" className="btn btn-success">Submit</button>
			</form>
		</div>
	</div>
</div>
    );
  }
}

export default Create;