import React, { Component } from 'react';
import './App.css';

import { Switch, Route } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';

import Home from "./components/Home.js";
import Create from "./components/Create.js";
import Edit from "./components/Edit.js";


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <main>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route exact path='/create' component={Create}/>
            <Route exact path='/edit/:id' component={Edit}/>                           
          </Switch>
        </main>
      </BrowserRouter>
    );
  }
}

export default App;